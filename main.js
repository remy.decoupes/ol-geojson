import 'ol/ol.css';
import GeoJSON from 'ol/format/GeoJSON';
import Map from 'ol/Map';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import View from 'ol/View';

const data = require('./data/countries.json')
var geojsonObject = data;

new Map({
  target: 'map-container',
  layers: [
    new VectorLayer({
      source: new VectorSource({
        features: (new GeoJSON()).readFeatures(geojsonObject,
        {
        dataProjection: 'EPSG:4326',
        featureProjection: 'EPSG:3857'
        })
      })
    })
  ],
  view: new View({
    center: [0, 0],
    zoom: 2
  })
})
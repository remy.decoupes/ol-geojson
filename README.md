# Installation
## Installation nodejs 
Permet d'exécuter le code javascript côté serveur (et non plus par le navigateur web client). Les tutoriels d'openlayer conseillent nodejs. Autre avantage, nodejs possède son propre gestionnaire de paquets/dépendances **npm**.

* Sous linux : ```apt install nodejs npm```
* Sous windows : 
    * Télécharger nodejs : https://nodejs.org/fr/download/
    * Exécuter le fichier MSI 

## Création d'un projet nodejs
Avec l'aide d'un terminal ou d'un console bash, se rendre dans un répertoire vierge qui contiendra le futur projet.

Initialiser le projet nodejs : 

```npm init```

## Installation d'openlayer
Installer la librairie Javascript **OpenLayer** (ol) via npm :

```npm install ol```

## installation de Parcel
Parcel est un "bundler" Javascript. Cela permet de rassembler l'ensemble de son code Javascript (et de ses dépendances= dans un seul fichier. L'application est plus comapcte et rapide.

```npm install --save-dev parcel-bundler```

## Récuperer les fichiers de ce dépôt git
Dans le répertoire utilisé précédemment pour initier le projet nodejs, tapez la commande suivante :

```git clone https://gitlab.irstea.fr/remy.decoupes/ol-geojson.git```

Si git n'est pas installé sur votre poste, vous pouvez télécharger l'archive du projet et la décompresser dans le répertoire.: 

# Configuration
## Arborescence
```
.
├── data
    └── countries.json : Le fichier geojson contenant les éléments. Récupéré du tutoriel openlayer
├── dist
├── index.html : Code HTML qui appelle le script généré à partir de main.js via parcel
├── main.js : Code Javascript qui sera packagé par parcel
├── node_modules : Répertoire contenant les différents modules (dépendances) utilisées par parcel pour construire le projet. (Gitignore)
├── README.md : les notes d'installation et d'utilisaiton
├── package.json : Configuration du projet nodejs (initialement créé par la commande npm init mais qui doit être complété ou écrasé par ce dépôt).
└── package-lock.json (Gitignore)
```

# Lancement
## Démarrer le serveur web
```npm start```

![image0002](img/image0002.png)
## Se connecter à la carte
Avec son navigateur client, aller à l'adresse suivante : 
http://localhost:1234/

![image0001](img/image0001.png)



